package com.example.anjali.kotlinfirebase

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.anjali.kotlinfirebase.util.FirestoreUtil
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.longSnackbar

class SignInActivity : AppCompatActivity() {
    private val SIGN_IN = 12
    private val signInproviders = listOf(AuthUI.IdpConfig.EmailBuilder()
            .setAllowNewAccounts(true)
            .setRequireName(true)
            .build())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        account_sign_in.setOnClickListener {
            val intent = AuthUI.getInstance().createSignInIntentBuilder()
                    .setAvailableProviders(signInproviders)
                    .setLogo(R.drawable.ic_settings_black_24dp)
                    .build()
            startActivityForResult(intent, SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SIGN_IN) {
            val reponse = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK) {
                val progressDialog = indeterminateProgressDialog("Setting up your account")
               FirestoreUtil.initCurrenctuserForFirstTime {
                   startActivity(intentFor<MainActivity>().newTask().clearTask())
                   progressDialog.dismiss()
               }
            }else if(resultCode==Activity.RESULT_CANCELED){
                if(reponse==null) return
                when(reponse.error?.errorCode){
                    ErrorCodes.NO_NETWORK ->
                            longSnackbar(constraint_layout,"No network");
                }
            }
        }
    }
}
