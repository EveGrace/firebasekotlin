package com.example.anjali.kotlinfirebase.model

data class User(val name:String,val bio:String,val profilePicturePath :String?) {

    //firestorage need a parameterless constuctor so this secondary constructor will call the primary User constructor
    constructor():this("","",null)
}